let express = require('express');
let installationController = require('../../app/api/installations/InstallationController');
let router = express.Router();

router.get('/emails/fetch',installationController.fetchEmail);
router.get('/fetch',installationController.fetchInstallation);

/* GET home page. */
router.post('/save', installationController.saveDevice);
router.post('/email', installationController.saveEmail);


module.exports = router;
