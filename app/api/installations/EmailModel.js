const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let EmailSchema = new Schema({
    email: {type: String, required: true, max: 200},
    createdAt: {type: Date, required: true, default: Date.now},
});


// Export the model
module.exports = mongoose.model('Email', EmailSchema);