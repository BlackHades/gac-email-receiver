const Installation = require('./InstallationModel');
const Email =  require('./EmailModel');

/**
 * Save Device Data
 * @param req
 * @param res
 * @returns {Promise<*|void|boolean>}
 */
let saveDevice = async (req, res) => {
    console.log("Req: " + JSON.stringify(req.body));
    // console.log("Res: " + JSON.stringify(res));
    // res.send({req: req,res: res})
   try{

       let check = await Installation.findOne({
           phoneType: req.body.phoneType,
           deviceId: req.body.deviceId
       });
       if(check != null)
           return res.json({status: 1, message: "Exists", data: JSON.stringify(check)});

       let ins = new Installation({
           uuid: req.body.uuid,
           phoneType: req.body.phoneType,
           deviceId: req.body.deviceId
       });
       console.log("Data: " + JSON.stringify(check));
       let data = await ins.save();
       return res.json({status: 1,message: "Saved",data: JSON.stringify(data)})

   }catch(e){
       console.log("Error Handler: " + JSON.stringify(e));
       return res.json({status: 0,message: e.toString(),data: JSON.stringify(e)})
   }
};

/**
 * Save Email
 * @param req
 * @param res
 * @returns {Promise<*|void|boolean>}
 */
let saveEmail = async (req, res) => {
    // console.log("Req: " + JSON.stringify(req));
    // console.log("Res: " + JSON.stringify(res));
    // res.send({req: req,res: res})
    try{
        let check = await Email.findOne({
            email: req.body.email
        });
        if(check != null)
            return res.send({status: 1, message: "Exists", data: JSON.stringify(check)});

        let ins = new Email({
            email: req.body.email,
        });
        console.log("Data: " + JSON.stringify(check));
        let data = await ins.save();
        return res.send({status: 1,message: "Saved",data: JSON.stringify(data)})

    }catch(e){
        console.log("Error Handler: " + JSON.stringify(e));
        return res.send({status: 0,message: e.toString(),data: JSON.stringify(e)})
    }
};


/**
 * Fetch All Emails
 * @param req
 * @param res
 * @returns {Promise<void>}
 */
let fetchEmail = async (req, res) => {
    try{
        let emails = await Email.find();
        return res.json({status:1, message:"",data: emails});
    }catch (e) {
        console.log("Error Handler: " + JSON.stringify(e));
        return res.send({status: 0,message: e.toString(),data: JSON.stringify(e)})
    }
};


/**
 * Fetch All Installations
 * @param req
 * @param res
 * @returns {Promise<void>}
 */
let fetchInstallation = async (req, res) => {
    try{
        let installations = await Installation.find();
        return res.json({status:1, message:"",data: installations});
    }catch (e) {
        console.log("Error Handler: " + JSON.stringify(e));
        return res.send({status: 0,message: e.toString(),data: JSON.stringify(e)})
    }
};
module.exports = {
    saveDevice,
    saveEmail,

    fetchEmail,
    fetchInstallation
};