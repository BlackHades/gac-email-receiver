const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let InstallationSchema = new Schema({
    uuid: {type: String, required: true},
    phoneType: {type: String, required: true},
    deviceId: {type: String, required: true},
    createdAt:{type: Date, required: true, default: Date.now}
});


// Export the model
module.exports = mongoose.model('Installation', InstallationSchema);